package linkedList

type List struct {
	first, current, last *Node
	len            int
}

func (l *List) Append(value interface{}) *List {
	if l.current == nil {
		l.init(value)
	} else {
		newNode := &Node{Value: value}
		newNode.prev = l.last
		l.last.next = newNode
		newNode.next = l.first
		l.first.prev = newNode
		l.last = newNode
		l.current = l.last
	}
	l.len++

	return l
}

func (l *List) init(value interface{}) {
	l.current = &Node{Value: value}
	l.first = l.current
	l.last = l.current
}

func (l *List) Len() int {
	return l.len
}

func (l *List) Get() *Node {
	return l.current
}

func (l *List) Next() *List {
	if (l.current.next != nil) {
		l.current = l.current.next
	}
	return l
}

func (l *List) Prev() *List {
	if (l.current.prev != nil) {
		l.current = l.current.prev
	}
	return l
}

func (l *List) Insert(value interface{}) *List {
	if l.current == nil {
		l.init(value)
		l.len++
	} else {
		newNode := &Node{Value: value}
		newNode.next = l.current.next
		l.current.next = newNode
		newNode.prev = l.current
		l.len++
	}
	return l
}

func (l *List) GetFirst() *Node {
	return l.first
}

func (l *List) GetLast() *Node {
	return l.last
}

func (l *List) Remove() *List {
	if l.len == 1 {
		l.len = 0
		l.current = nil
	} else if l.current != nil {
		l.current.prev.next = l.current.next
		l.current.next.prev = l.current.prev
		l.current = l.current.next

		if l.len > 0 {
			l.len--
		}
	}

	return l
}