package linkedList

import (
	"testing"
	"fmt"
)

var first, second, third, fourth string


func TestList_Len(t *testing.T) {
	list := List{}
	if list.Len() != 0 {
		t.Error("expected Len() to return 0 for an empty list")
	}
}

func initFixtures()  {
	first = "First"
	second = "Second"
	third = "Third"
	fourth = "Fourth"
}

func initList() List {
	list := List{}
	initFixtures()

	list.Append(first)
	list.Append(second)
	list.Append(third)
	return list
}

func TestList_Append(t *testing.T) {
	list := List{}
	testSubject := "First"
	list.Append(testSubject)
	if list.Len() != 1 {
		t.Error("expected Len() to return 1 after an appending")
	}
	if list.Get().Value != testSubject {
		t.Error("expected Get() to an appended node")
	}

	list.Append(second)
	if list.Get().Value != second {
		t.Error("expected Append to move cursor to the newly added one")
	}
}

func TestList_Get(t *testing.T) {
	list := List{}
	node := list.Get()
	if node != nil {
		t.Error(fmt.Sprintf("expected nil, but got %s", node.Value))
	}

	list = initList()
	list.Prev()
	node = list.Get()
	if node.Value != second {
		t.Error(fmt.Sprintf("expected %s, but got %s", second, node.Value))
	}
}

func TestList_Insert(t *testing.T) {
	list := initList()
	value := list.Prev().Insert(fourth).Next().Get().Value

	if value != fourth {
		t.Error(fmt.Sprintf("expected %s to be inserted between second and third, but got %s", fourth, value))
	}
	if list.Len() != 4 {
		t.Error(fmt.Sprintf("expected length to be %s, but got %s", 4, list.Len()))
	}
}

func TestList_Prev(t *testing.T) {
	list := initList()
	value := list.Prev().Get().Value
	if value != second {
		t.Error(fmt.Sprintf("expected %s, but got %s", second, value))
	}
}

func TestList_Prev_of_the_first_node_should_be_the_last(t *testing.T) {
	list := initList()
	value := list.Prev().Prev().Prev().Get().Value
	if value != third {
		t.Error(fmt.Sprintf("expected %s, but got %s", third, value))
	}
}

func TestList_Next(t *testing.T) {
	list := initList()
	value := list.Prev().Next().Get().Value
	if value != third {
		t.Error(fmt.Sprintf("expected %s, but got %s", third, value))
	}
}

func TestList_Next_of_the_last_node_should_be_the_first(t *testing.T) {
	list := initList()
	value := list.Next().Get().Value
	if value != first {
		t.Error(fmt.Sprintf("expected %s, but got %s", first, value))
	}
}

func TestList_GetFirst(t *testing.T) {
	list := initList()
	value := list.GetFirst().Value
	if value != first {
		t.Error(fmt.Sprintf("expected %s, but got %s", first, value))
	}
}

func TestList_GetLast(t *testing.T) {
	list := initList()
	value := list.Prev().GetLast().Value
	if value != third {
		t.Error(fmt.Sprintf("expected %s, but got %s", third, value))
	}
}

func TestList_Remove(t *testing.T) {
	list := initList()
	list.Prev().Remove()
	if list.Len() != 2 {
		t.Error(fmt.Sprintf("expected length to be %s, but got %s", 2, list.Len()))
	}

	value := list.Get().Value
	if value != third {
		t.Error(fmt.Sprintf("expected the current node to be the next, but got %s", value))
	}
}

func TestList_Remove_till_empty(t *testing.T) {
	list := initList()
	list.Remove()
	list.Remove()
	value := list.Get().Value
	if value != second {
		t.Error(fmt.Sprintf("expected %s, but got %s", second, value))
	}
	node := list.Remove().Get()
	if node != nil {
		t.Error(fmt.Sprintf("expected %s, but got %s", nil, node.Value))
	}
}
